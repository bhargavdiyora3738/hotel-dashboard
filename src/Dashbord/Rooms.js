import React from 'react';
import './Rooms.css';
import Card from './Card';


export const Rooms = (props) => {
    return (
        <div>
            <table className="roomstable">
                <tbody>
                    <tr>
                        <td>{props.roomno}</td>
                        <td>{props.roomtype}</td>
                        
                    </tr>
                    
                </tbody>
            </table>
            
        </div>
    )
}
export default Rooms;