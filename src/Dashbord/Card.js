import React, { useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { v4 as uuidv4 } from 'uuid';
import './Card.css'

const itemsFromBackend = [
  { id:"r1", content: "Bhargav D" },
 ];
const itemRoom=[{id:"r2", content:"Akshay D"}]
const columnsFromBackend = {
  ["r1"]: {
   
    items: itemsFromBackend
  },[uuidv4()]: {
    
    items: []
  },
  [uuidv4()]: {
   
    items: []
  },
  [uuidv4()]: {
 
    items: []
  },
  [uuidv4()]: {
  
    items: []
  },
  [uuidv4()]: {
    
    items: []
  },
  [uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: itemRoom
  },
  [uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },
  [uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },
  [uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },
  [uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },
  [uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },
  [uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },
  [uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },[uuidv4()]: {
    
    items: []
  },
  [uuidv4()]: {
    
    items: []
  },
};


const onDragEnd = (result, columns, setColumns) => {
  if (!result.destination) return;
  const { source, destination } = result;

  if (source.droppableId !== destination.droppableId) {
    const sourceColumn = columns[source.droppableId];
    const destColumn = columns[destination.droppableId];
    const sourceItems = [...sourceColumn.items];
    const destItems = [...destColumn.items];
    const [removed] = sourceItems.splice(source.index, 1);
    destItems.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: {
        ...sourceColumn,
        items: sourceItems
      },
      [destination.droppableId]: {
        ...destColumn,
        items: destItems
      }
    });
  } else {
    const column = columns[source.droppableId];
    const copiedItems = [...column.items];
    const [removed] = copiedItems.splice(source.index, 1);
    copiedItems.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: {
        ...column,
        items: copiedItems
      }
    });
  }
};

function Card() {
  const [columns, setColumns] = useState(columnsFromBackend);
  return (
    <div className="box">
      <DragDropContext
        onDragEnd={result => onDragEnd(result, columns, setColumns)}
      >
        {Object.entries(columns).map(([columnId, column], index) => {
          return (
            <div 
              key={columnId}
            >
              
              <div style={{ margin: 4 }}>
                <Droppable droppableId={columnId} key={columnId}>
                  {(provided, snapshot) => {
                    return (
                      <div style={{}}>
                      <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={{
                          background: snapshot.isDraggingOver
                            ? "lightblue"
                            : "lightgray",
                          
                          width: 100,
                          minHeight: 100,
                          marginLeft:20
                          
                         

                        }}
                      >
                        {column.items.map((item, index) => {
                          return (
                            <Draggable
                              key={item.id}
                              draggableId={item.id}
                              index={index}
                            >
                              {(provided, snapshot) => {
                                return (
                                  <div
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    style={{
                                      userSelect: "none",
                                      padding: 16,
                                      margin: "0 0 8px 0",
                                      minHeight: "67px",
                                      backgroundColor: snapshot.isDragging
                                        ? "#263B4A"
                                        : "#456C86",
                                      color: "white",
                                      ...provided.draggableProps.style
                                    }}
                                  >
                                    {item.content}
                                  </div>
                                );
                              }}
                            </Draggable>
                          );
                        })}
                        {provided.placeholder}
                      </div></div>
                    );
                  }}
                </Droppable>
              </div>
            </div>
          );
        })}
      </DragDropContext>
    </div>
  );
}

export default Card;



